<?php

namespace Database\Seeders;


use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $categories = [
            ['name'=>'Immobili'],
            ['name'=>'Arredamento'],
            ['name'=>'Motori'],
            ['name'=>'Abbigliamento'],
            ['name'=> 'Fotografia'],
            ['name'=>'Musica'],
            ['name'=>'Sport'],
            
        ];

        foreach ($categories as $category) {
            DB::table('categories')->insert([
                'name'=> $category ['name'],

                //!carbon
                'created_at'=>Carbon::now(),
                'updated_at'=>Carbon::now(),
            ]);
            
        }
    }
}
