<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcements', function (Blueprint $table) {

             //settaggio chiave esterna
             $table->unsignedBigInteger('user_id')->after('price')->nullable();

             //vincolo referenziale delle chiavi
             $table->foreign('user_id')->references('id')->on('announcements');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcements', function (Blueprint $table) {
            
            //distruzione del vincolo referenziale
            $table->dropForeign(['user_id']);
            //distruzione della colonna
            $table->dropColumn('user_id');
        });
    }
};
