<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('announcements', function (Blueprint $table) {

            //settaggio chiave esterna
            $table->unsignedBigInteger('category_id')->after('user_id')->nullable();

            //vincolo referenziale delle chiavi
           $table->foreign('category_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcements', function (Blueprint $table) {
            
             //!distruzione del vincolo referenziale
             $table->dropForeign(['category_id']);
             //!distruzione colonna
             $table->dropColumn('category_id');
        });
    }
};
