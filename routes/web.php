<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\AnnouncementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class , 'home'])->name('goHome');

// ROTTE PER ANNUNCI

Route::get('/announcement/new' , [AnnouncementController::class , 'create'])->name('insert_ann');
Route::post('/announcement/create', [AnnouncementController::class, 'store'])->name('store_ann');
Route::get('/announcemen/category/{name}/{category_id}', [PublicController::class, 'byCategory'])-> name('byCategory');
Route::get('/detail/{announcementID}', [AnnouncementController::class, 'show'])->name('detail_ann');


// ROTTA PER BARRA DI RICERCA
Route::get('/search', [PublicController::class, 'search'])->name('search');


//!rotte per revisore
Route::get('/revisor/home' , [RevisorController::class, 'index'])->name('revisor.home');
Route::post('/revisor/announcement/{id}/accept', [RevisorController::class, 'accept'])->name('revisor.accept');
Route::post('/revisor/announcement/{id}/reject', [RevisorController::class, 'reject'])->name('revisor.reject');
