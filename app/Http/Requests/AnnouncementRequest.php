<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=> 'required|min:3',
            'description'=> 'required|min:10',
            'price'=> 'required|numeric',
        ];
    }
    public function messages() {
        return
        [
            'title.required'=> 'Titolo assente',
            'title.min'=> 'Il titolo deve contenere almeno 3 caratteri',
            'description.required'=> 'Descrizione assente',
            'description.min'=> 'La descrizione deve contenere almeno 10 caratteri',
            'price.required'=> 'Inserisci il prezzo in formato numerico',
        ];
    }

}
