<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PublicController extends Controller
{
    public function home() {

        $announcements= Announcement::where('is_accepted', true)->orderBy('created_at', 'DESC')->take(6)->get();
        return view('welcome', compact('announcements'));
    }

    public function byCategory($name, $category_id){

        // filtra le categorie
        $category= Category::find($category_id);

        // passa le categorie filtrate con gli annunci allegati ad esse, e le impagina per 5 elementi a vista
        $announcements= $category-> announcements()
            ->where('is_accepted', true)
            ->orderBy('created_at', 'desc')
            -> paginate(5);
        return view('announcement.byCategory', compact('category', 'announcements'));
    }
    
    public function search(Request $request){

        $q = $request->input('q');
        $announcements = Announcement::search($q)->query(function ($builder){
            $builder->with(['category']);
        })->get();

        return view('announcement.search_results', compact('q', 'announcements'));

    }

}
