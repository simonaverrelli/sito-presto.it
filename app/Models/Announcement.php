<?php

namespace App\Models;

use App\Models\User;
use App\Models\Category;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Scout\Searchable;

class Announcement extends Model
{
    use HasFactory;
    use Searchable;


    protected $fillable =[
        'title',
        'description',
        'price',
        'category_id'
        
    ];

    //   funzione di relazione con gli utenti
    public function user(){
        
        return $this->belongsTo(User::class);
    }


    public function category(){
       return $this->belongsTo(Category::class);
    }


    //! funzione Contatore Degli Annunci Da Revisionare
    static public function ToBeRevisionedCount(){
        
        return Announcement::where('is_accepted', null)->count();
    }


    public function toSearchableArray()
    {
        $announcement='';
        $category=$this->category;
        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'price' => $this->price,
            'more' => 'annuncio',
            'announcement' => $announcement,
            'category' => $category
        ];
    
    
        return $array;
    }
}
