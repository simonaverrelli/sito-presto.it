<nav class="navbar navbar-expand-lg  general-nav">
    <div class="container-fluid">
      <a class="navbar-brand text-white" href="{{route('goHome')}}">Presto</a>
      <button class="navbar-toggler nav-togg" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-ellipsis-h"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="{{route('goHome')}}">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('insert_ann')}}">Inserisci Annuncio</a>
          </li>
        </ul>
      
     
        @guest
     
     
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="{{route('register')}}">Registrati</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('login')}}">Accedi</a>
          </li>
        </ul>
       
        @else
        
        <ul class="navbar-nav mb-2 mb-lg-0 ">
          <li class="nav-item">
            <a type="button"class="nav-link">Benvenutə {{Auth::user()->name}}</a>
          </li>
          <li class="nav-item">
            <a class="nav-link nav-link active mx-3 nav_a" href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">Esci</a>
             <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none"> @csrf </form>
          </li>
         
        </ul>
      </div>

      @endguest


    </div>
  </nav>