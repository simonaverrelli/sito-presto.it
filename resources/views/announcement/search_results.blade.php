<x-layout>
    {{-- <div class="container">
        <h1 class="text-center bg-warning my-5 fw-bold">Risultato ricerca : {{ $q }}</h1>
        <div class="row justify-content-evenly"> --}}


            <div class="container mt-5">
                <div class="row ">
                    <div class="col-12 ">
                      <h1 class=" text-center mb-3 mt-5">Risultato ricerca:  <span class="tc-main fw-bold">{{$q}}</span></h1>
                    </div>
                  </div>
        
                <div class="row">
                    @foreach ($announcements as $announcement)
                        <div class="col-12 col-sm-6 col-lg-4 my-3  ">
                            <div class=" product-card py-3 ">
                                <div class="product-image">
                                    <div class="overlay"></div>
                                    <img src="https://picsum.photos/400" class="card-img-top img-fluid" alt="...">
                                </div>
                                
                                <div class="product-detail px-2 card-body">
                                    <h4 class="card-title text-truncate">{{ $announcement->title }}</h4>
                                    <p class="card-text text-truncate">{{ $announcement->description }}</p>
                                    <div class="d-flex justify-content-between align-items-center my-3">
                                    <p class="card-text m-0">{{ $announcement->price }}</p>
                                    <a class="cat-card-link" href="{{ route('byCategory', [$announcement->category->name, $announcement->category->id]) }}"><span
                                            class="fw-bold  m-0"> </span>{{ $announcement->category->name }}
                                     </a>
                                    </div>
                                    <div class="d-flex justify-content-between align-items-center my-4">
                                    <p class="card-text  m-0 ">Pubblicato da: {{ $announcement->user->name }}</p>
                                    <p class="card-text m-0"><span class="fw-bold">{{ $announcement->date }}
                                        </span>{{ date_format($announcement->created_at, 'd/m/y') }}</p>
                                    </div>
                                    <a href="{{ route('detail_ann', $announcementID = $announcement['id']) }}"
                                        class="btn btn-card mt-3 py-2 px-3 w-100 ">Vai al Prodotto</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>






</x-layout>