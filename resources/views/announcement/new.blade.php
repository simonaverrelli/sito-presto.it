<x-layout>
    <div class="container">
        <h1 class=" text-center mt-5 mb-2">Pubblica il tuo Annuncio</h1>
        <div class="row  h-100 align-items-center  justify-content-center">
            <div class="col-12  col-md-12 col-lg-5">
                <lottie-player src="https://assets8.lottiefiles.com/packages/lf20_webygbhv.json"  background="transparent"  speed="1"  class="lottie " loop  autoplay></lottie-player>
              </div>
            <div class="col-12 col-md-12 col-lg-6">
                <form method="POST" action="{{ route('store_ann') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">Titolo</label>
                        <input type="text" class="form-control  input-form-ads" name="title">
                        {{-- MESSAGGIO DI ERRORE TITLE --}}
                        @error('title')
                            <div class="alert alert-danger"> {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Descrizione</label>
                        <input type="text" class="form-control input-form-ads" name="description">
                        {{-- MESSAGGIO DI ERRORE DESCRIPTION --}}
                        @error('description')
                            <div class="alert alert-danger">{{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label class="form-label">Prezzo</label>
                        <input type="number" class="form-control input-form-ads" name="price">
                        {{-- MESSAGGIO DI ERRORE --}}
                        @error('price')
                            <div class="alert alert-danger provaErrore">{{ $message }}</div>
                        @enderror
                    </div>
                    <select name="category" class="form-select cat-select mb-3  input-form-ads ">
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>


                    <button type="submit" class="btn insert-btn">Inserisci</button>
                </form>

            </div>
            
        </div>
    </div>








</x-layout>
