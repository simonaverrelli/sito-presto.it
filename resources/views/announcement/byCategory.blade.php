<x-layout>



    <h1>Annunci per {{$category->name}}</h1>

    <div class="container">
        <div class="row justify-content-evenly">

            @foreach ($announcements as $announcement)
            
                <div class=" product-card ">
                  <div class="product-image">
                      <div class="overlay"></div>
                      <img src="https://picsum.photos/400" class="card-img-top img-fluid" alt="...">
                  </div>
                  
                  <div class="product-detail px-2  card-body">
                      <h4 class="card-title text-truncate">{{ $announcement->title }}</h4>
                      <p class="card-text text-truncate">{{ $announcement->description }}</p>
                      <div class="d-flex justify-content-between align-items-center my-3">
                      <p class="card-text m-0">{{ $announcement->price }}</p>
                      <a class="cat-card-link" href="{{ route('byCategory', [$announcement->category->name, $announcement->category->id]) }}"><span
                              class="fw-bold  m-0"> </span>{{ $announcement->category->name }}
                       </a>
                      </div>
                      <div class="d-flex justify-content-between align-items-center my-4">
                      <p class="card-text  m-0 ">Pubblicato da: {{ $announcement->user->name }}</p>
                      <p class="card-text m-0"><span class="fw-bold">{{ $announcement->date }}
                          </span>{{ date_format($announcement->created_at, 'd/m/y') }}</p>
                      </div>
                      <a href="{{ route('detail_ann', $announcementID = $announcement['id']) }}"
                          class="btn btn-card mt-3 py-2 px-3 w-100 ">Vai al Prodotto</a>
                  </div>
              </div>

        
            @endforeach
        </div>
    </div>











</x-layout>