<x-layout>

    <div class="container my-5">
      <div class="row justify-content-center">
          <div class="col-md-5">
              <div class="row">
                  <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
                      <div class="carousel-indicators">
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
                      </div>
                      <div class="carousel-inner">
                        <div class="carousel-item active">
                          <img src="https://picsum.photos/300/201" class="d-block w-100" alt="not found">
                        </div>
                        <div class="carousel-item">
                          <img src="https://picsum.photos/300/202" class="d-block w-100" alt="not found">
                        </div>
                        <div class="carousel-item">
                          <img src="https://picsum.photos/300/203" class="d-block w-100" alt="not found">
                        </div>
                      </div>
                      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                      </button>
                      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                      </button>
                    </div>
              </div>
              <div class="row mt-5">
                  <div class="col-3">
                      <img class="img-fluid" src="/img/very space.jpg" alt="">
                  </div>
                  <div class="col-3">
                      <img class="img-fluid" src="/img/very space.jpg" alt="">
                  </div>
                  <div class="col-3">
                      <img class="img-fluid" src="/img/very space.jpg" alt="">
                  </div>
                  <div class="col-3">
                      <img class="img-fluid" src="/img/very space.jpg" alt="">
                  </div>
              </div>
          </div>
          <div class="col-md-7 align-items-center">
              <h1 class="fw-bold my-5"></span>{{$announcement->title }}</h1>
              <p>{{$announcement->description }}</p>
              <h2 class="text-uppercase fw-bold">€ {{$announcement->price }}</h2>
              <button class="btn_detail_cart text-white btn  my-5 fw-bold">Aggiungi al carrello</button>
              {{-- @guest
              @else    
              <a href="{{route('editAnnouncement', compact('announcement'))}}" class="btn_detail_edit btn text-white fw-bold mx-5">Modifica</a>
              @endguest --}}
  
          </div>
  </x-layout>