<x-layout>
    @if ($announcement)
    
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Annuncio # {{ $announcement->id }}</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2">
                                <h3>Utente :</h3>
                            </div>
                            <div class="col-md-10">
                                <h3># {{ $announcement->user->id }}, {{ $announcement->user->name }}, {{ $announcement->user->email }}</h3>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <h3>Titolo :</h3>
                            </div>
                            <div class="col-md-10">
                                <h3>{{ $announcement->title }}</h3>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <h3>Descrizione :</h3>
                            </div>
                            <div class="col-md-10">
                                <h3>{{ $announcement->description }}</h3>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <h3>Prezzo :</h3>
                            </div>
                            <div class="col-md-10">
                                <h3>{{ $announcement->price }}</h3>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-md-2">
                                <h3>Immagini :</h3>
                            </div> --}}
                            {{-- <div class="col-md-10">
                                @foreach ($announcement->images as $image )
                                <div class="row mb-2">
                                    <div class="col-md-4">
                                        <img src="{{$image->getUrl(300, 150)}}" class="rounded" alt="">
                                    </div>
                                    <div class="col-md-8">
                                        Adult: {{ $image->adult }} <br>
                                        spoof: {{ $image->spoof }} <br>
                                        medical: {{ $image->medical }} <br>
                                        violence: {{ $image->violence }} <br>
                                        racy: {{ $image->racy }} <br>
                                        
                                        <b>labels</b> <br>
                                        <ul>
                                            @if ($image->labels)
                                            @foreach ($image->labels as $label)
                                            <li>{{$label}}</li>
                                            @endforeach
                                            @endif
                                        </ul>
                                        
                                        {{$image->id}} <br>
                                        {{$image->file}} <br>
                                        {{ Storage::url($image->file) }}
                                    </div>
                                </div>
                                {{-- <div class="row mb-2">
                                    <div class="col-md-4">
                                        <img src="" alt="">
                                    </div>
                                    <div class="col-md-8">
                                        ... ... ...
                                    </div>
                                </div>                              --}}
                                {{-- @endforeach
                            </div>
                        </div> --}}
                        
                    </div>
                </div>
                
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <div class="col-md-6 text-right">
                <form method="POST" action="{{ route('revisor.reject', $announcement->id) }}">
                    @csrf
                    <button type="submit" class="btn btn-danger">Rifiuta</button>
                </form>
            </div>
        </div>
        <div class="col-md-6 text-right">
            <form method="POST" action="{{ route('revisor.accept', $announcement->id) }}">
                @csrf
                <button type="submit" class="btn btn-success">Accetta</button>
            </form>
        </div>
    </div>
</div>
@else
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <h1>Non Ci Sono Più Annunci Da Revisionare</h1>
        </div>
    </div>
</div>
@endif
</x-layout>