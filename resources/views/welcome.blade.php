<x-home_layout>
  
    {{-- HEADER --}}
    <div class="container-fluid header ">
        <div class="row h-100 align-items-center text-white justify-content-center">
            <div class="col-12 col-md-6 text-center  header-title">
                <h1 class="display-3 fw-bold">Presto <span class="tc-main">.</span>it</h1>
                <p id="p-header">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                <form method="GET" action="{{ route('search') }}" class="input-group my-4 input-search">
                    <input name="q" type="search" class="form-control" placeholder="Cerca tra i nostri annunci"
                        aria-label="cerca tra i nostri annunci" aria-describedby="button-addon2">
                    <button class="btn btn-outline-secondary" type="submit" id="button-addon2">
                        <i class="fa-solid fa-magnifying-glass"></i>
                    </button>
                </form>
                {{-- <button type="button " class="btn header-btn px-5 py-2 mt-3">Scopri i nostri annunci</button> --}}
            </div>
        </div>
    </div>

    
    {{-- CATEGORY SECTION --}}

    <div class="container category-container ">
        <div class="row mb-5 ">
            <div class="col-12  ">
                <h3 class="fs-2 text-uppercase line-decoration mt-5 mb-1 text-end">Le nostre <span
                        class="tc-main">Categorie</span>
                </h3>
            </div>
        </div>
        <div class="row justify-content-evenly">
            @foreach ($categories as $category)
                <div class="col-12 col-sm-6 col-md-4 col-lg-3 my-3 ">
                    <div class="card-cat">
                        <div class="card-cat-inner">
                            <div class="card-cat-inner-front">
                                <p class="card-cat-title"> {{ $category->name }}</p>
                                <p class="mb-0 cat-p">Per tutti gli annunci</p>
                                <i class="fas fa-angle-double-right cat-icon"></i>

                            </div>

                            <div class="card-cat-inner-back">

                                <a class="btn stretched-link cat-btn"
                                    href="{{ route('byCategory', [$category->name, $category->id]) }}">Clicca qui</a>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>


    {{-- ULTIMI ANNUNCI --}}

    <div class="container mt-5">
        <div class="row ">
            <div class="col-12 ">
              <h3 class="fs-2 text-uppercase line-decoration mb-3 mt-5">Gli ultimi <span class="tc-main fw-bold">annunci</span></h3>
            </div>
          </div>
    
    <div class="container">
        <div class="row">

            <div class="col-12">
                <!-- Slider main container -->
                <div class="swiper">
                    <!-- Additional required wrapper -->
                    <div class="swiper-wrapper">
                        <!-- Slides -->
                        @foreach ($announcements as $announcement)
                        <div class="swiper-slide">
                            <div class="container">

                                <div class="row justify-content-evenly">
                                  
                                    <div class="col-12 my-3  ">
                                        <div class=" product-card py-3 ">
                                            <div class="product-image">
                                                <div class="overlay"></div>
                                                <img src="https://picsum.photos/400" class="card-img-top img-fluid" alt="...">
                                            </div>
                                            
                                            <div class="product-detail px-2 card-body">
                                                <h4 class="card-title text-truncate">{{ $announcement->title }}</h4>
                                                <p class="card-text text-truncate">{{ $announcement->description }}</p>
                                                <div class="d-flex justify-content-between align-items-center my-3">
                                                <p class="card-text m-0">{{ $announcement->price }}</p>
                                                <a class="cat-card-link" href="{{ route('byCategory', [$announcement->category->name, $announcement->category->id]) }}"><span
                                                        class="fw-bold  m-0"> </span>{{ $announcement->category->name }}
                                                 </a>
                                                </div>
                                                <div class="d-flex justify-content-between align-items-center my-4">
                                                <p class="card-text  m-0 ">Pubblicato da: {{ $announcement->user->name }}</p>
                                                <p class="card-text m-0"><span class="fw-bold">{{ $announcement->date }}
                                                    </span>{{ date_format($announcement->created_at, 'd/m/y') }}</p>
                                                </div>
                                                <a href="{{ route('detail_ann', $announcementID = $announcement['id']) }}"
                                                    class="btn btn-card mt-3 py-2 px-3 w-100 ">Vai al Prodotto</a>
                                            </div>
                                        </div>
                                    </div>
                             


                                </div>

                            </div>
                        </div>
                        @endforeach

                    </div>
                    <!-- If we need pagination -->
                    {{-- <div class="swiper-pagination"></div> --}}
                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
              <div class="swiper-button-next"></div>


                </div>

            </div>

        </div>
    
    </div>

  


   

    @if (session('announcement.created.success'))
        <div class="alert alert-success">
            bella zi
        </div>
    @endif
    
    @if (session('access.denied.revisor.only'))
  <div class="alert alert-danger">
    accesso non consentito. solo per revisori
  </div>
  @endif

</x-home_layout>
