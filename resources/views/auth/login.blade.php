<x-auth-layout>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif



    <div class="container h-100  align-items-center  justify-content-center ">
        {{-- <h1 class="auth-title text-center mt-5"> Accedi subito su <br> Presto<span class="tc-main">.</span>it</h1> --}}
        <div class="row h-100 align-items-center  justify-content-center ">
            <div class="col-12 col-md-4 text-center ">
                <button class=" btn-backHome my-5">
                    <div class="link">
                        <a href="{{ route('goHome') }}" class="btn-backHome-box">
                            <i class="fa-solid fa-house-chimney-window"></i>
                        </a>
                    </div>
                    <span>take me home</span>
                </button>
            </div>


            <div class="col-12 col-md-8 col-lg-6  text-center">
                <div class="auth-form justify-content-center align-items-center text-center">
                    <i class="fa-solid fa-circle-user auth-icon "></i>
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="mb-3">
                            <label class="form-label ">Email</label>
                            <input type="email" class="form-control input-auth" name="email">
                        </div>
                        <div class="mb-3">
                            <label class="form-label">Password</label>
                            <input type="password" class="form-control  input-auth" name="password">
                        </div>
                        <button type="submit" class="btn mt-4 auth-btn">Accedi</button>
                    </form>
                    <p class="mt-3">Non hai un Account? <a href="{{ route('register') }}"
                            class="auth-link">Registrati</a>
                    </p>
                </div>
            </div>

        </div>
    </div>
    
  




</x-auth-layout>
