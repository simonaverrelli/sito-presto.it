<x-auth-layout>

    @if ($errors->any())
    <div class="alert alert-danger">
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    
    <div class="container">
      {{-- <h1 class="auth-title text-center mt-5"> Registrati subito su <br> Presto<span class="tc-main">.</span>it</h1> --}}
        <div class="row h-100 align-items-center justify-content-center">
          <div class="col-12 col-md-4 text-center ">
            <button class=" btn-backHome my-5">
                <div class="link">
                    <a href="{{ route('goHome') }}" class="btn-backHome-box">
                        <i class="fa-solid fa-house-chimney-window"></i>
                    </a>
                </div>
                <span>take me home</span>
            </button>
        </div>
            <div class="col-12 col-md-8 col-lg-6 text-center">
              <div class="auth-form justify-content-center align-items-center text-center">
                <i class="fa-solid fa-circle-user auth-icon "></i>

                <form method="POST" action="{{route('register')}}">
                    @csrf
                    <div class="mb-3">
                      <label  class="form-label">Nome Utente</label>
                      <input type="text" class="form-control input-auth" name="name">
                    </div>
                    <div class="mb-3">
                        <label  class="form-label">Email address</label>
                        <input type="email" class="form-control input-auth" name="email" >
                    </div>
                    <div class="mb-3">
                        <label  class="form-label">Password</label>
                        <input type="password" class="form-control input-auth" name="password">
                    </div>
                    <div class="mb-3">
                      <label class="form-label">Conferma Password</label>
                      <input type="password" class="form-control input-auth"  name="password_confirmation">
                    </div>
                    <button type="submit" class="btn mt-4 auth-btn">Registrati</button>
                  </form>
                  <p class="mt-3 mb-0">Hai già un Account? <a href="{{ route('login') }}"
                    class="auth-link">Accedi</a>
            </p>
              </div>
            </div>
        </div>
    </div>

    





</x-auth-layout>